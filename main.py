import logging
from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.filters import Text
from aiogram.utils.exceptions import MessageNotModified
from os import getenv
from sys import exit
from random import randint
from contextlib import suppress


# Токен берётся из переменной окружения
token = getenv("BOT_TOKEN")
if not token:
    exit("Error: no token provided")

bot = Bot(token=token)
dp = Dispatcher(bot)
logging.basicConfig(level=logging.INFO)

user_data = {
    "mainFood": "",
}

# ----------
@dp.message_handler(commands="help")
async def cmd_help(message: types.Message):
    await message.answer(
        f"<u>Команды</u>:\n\n/start - текстовый квест \n"
        f"/special_buttons - специальные кнопки \n"
        f"/inline_url - тестирование функционала \n"
        f"/numbers - тестирование функционала \n"
        f"/random - тестирование функционала \n", parse_mode="HTML")


@dp.message_handler(commands="start")
async def cmd_start(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Бутерброды!", "Нормальная еда"]
    keyboard.add(*buttons)
    await message.answer("Привет!\nИспользуй /help, "
                         "чтобы узнать список доступных команд!\n"
                         "Добро пожаловать в квест 'Готовим завтрак вместе с Ксюшей',\n "
                         "давай определимся с основным блюдом", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Бутерброды!")
async def buters(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Кофе", "Чай"]
    keyboard.add(*buttons)
    user_data["mainFood"] = "but"
    await message.reply("Отличный выбор, быстро и вкусно.\n"
                        "Определимся с напитком!", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Нормальная еда")
async def goodFood(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Что то яичное", "Каша"]
    keyboard.add(*buttons)
    await message.reply("Что именно?", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Каша")
async def kasha(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Бутерброды!", "Что то яичное"]
    keyboard.add(*buttons)
    await message.reply("У Ксюши нет времени варить кашу!\n"
                        "Нужно выбрать что то другое", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Что то яичное")
async def eggs(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Яичница", "Яйцо пашот"]
    keyboard.add(*buttons)
    await message.reply("Что именно?", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Яйцо пашот")
async def eggs2(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Яичница", "Бутерброды!"]
    keyboard.add(*buttons)
    await message.reply("По вашему мнению Ксюша умеет готовить яйцо пашот?!\n"
                        "Давайте что-нибудь попроще", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Яичница")
async def eggs1(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Чай", "Кофе"]
    keyboard.add(*buttons)
    user_data["mainFood"] = "egg"
    await message.reply("Хороший выбор.\n"
                        "Определимся с напитком!", reply_markup=keyboard)

@dp.message_handler(lambda message: message.text == "Чай")
async def tea(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Да", "Нет"]
    keyboard.add(*buttons)
    await message.reply("Хороший выбор.\n"
                        "Добавим сахар?", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Кофе")
async def coffe(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Чай"]
    keyboard.add(*buttons)
    await message.reply("К сожалению у Ксюши нет молока в холодильнике, "
                        "а пить кофе без молока мы не будем!\n"
                        "Остался один вариант", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Да")
async def yes(message: types.Message):
    photo = open('but.jpg', 'rb')
    if user_data["mainFood"] == "egg":
        photo = open('egg.jpg', 'rb')
    if user_data["mainFood"] == "but":
        photo = open('but.jpg', 'rb')
    await message.reply("Ай-яй-яй, употребление сахара должно быть ограничено! "
                        "Сахар вреден, подумай об этом.\n"
                        "Завтрак готов\n"
                        "Спасибо за участие в квесте!", reply_markup=types.ReplyKeyboardRemove())
    await bot.send_photo(message.chat.id, photo)

@dp.message_handler(lambda message: message.text == "Нет")
async def no(message: types.Message):
    photo = open('but.jpg', 'rb')
    if user_data["mainFood"] == "egg":
        photo = open('egg.jpg', 'rb')
    if user_data["mainFood"] == "but":
        photo = open('but.jpg', 'rb')
    await message.reply("Правильно!\n"
                        "Завтрак готов!\n"
                        "Спасибо за участие в квесте!", reply_markup=types.ReplyKeyboardRemove())

    await bot.send_photo(message.chat.id, photo)




# ----------

@dp.message_handler(commands="special_buttons")
async def cmd_special_buttons(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(types.KeyboardButton(text="Запросить геолокацию", request_location=True))
    keyboard.add(types.KeyboardButton(text="Запросить контакт", request_contact=True))
    keyboard.add(types.KeyboardButton(text="Создать викторину",
                                      request_poll=types.KeyboardButtonPollType(type=types.PollType.QUIZ)))
    await message.answer("Выберите действие:", reply_markup=keyboard)

# ----------

@dp.message_handler(commands="inline_url")
async def cmd_inline_url(message: types.Message):
    buttons = [
        types.InlineKeyboardButton(text="GitHub", url="https://github.com"),
        types.InlineKeyboardButton(text="Оф. канал Telegram", url="tg://resolve?domain=telegram")
    ]
    keyboard = types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(*buttons)
    await message.answer("Кнопки-ссылки", reply_markup=keyboard)

# ----------

@dp.message_handler(commands="random")
async def cmd_random(message: types.Message):
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="Нажми меня", callback_data="random_value"))
    await message.answer("Нажмите на кнопку, чтобы бот отправил число от 1 до 10", reply_markup=keyboard)


@dp.callback_query_handler(text="random_value")
async def send_random_value(call: types.CallbackQuery):
    await call.message.answer(str(randint(1, 10)))
    await call.answer(text="Спасибо, что воспользовались ботом!", show_alert=True)


def get_keyboard():
    buttons = [
        types.InlineKeyboardButton(text="-1", callback_data="num_decr"),
        types.InlineKeyboardButton(text="+1", callback_data="num_incr"),
        types.InlineKeyboardButton(text="Подтвердить", callback_data="num_finish")
    ]
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard


async def update_num_text(message: types.Message, new_value: int):
    with suppress(MessageNotModified):
        await message.edit_text(f"Укажите число: {new_value}", reply_markup=get_keyboard())


@dp.message_handler(commands="numbers")
async def cmd_numbers(message: types.Message):
    user_data[message.from_user.id] = 0
    await message.answer("Укажите число: 0", reply_markup=get_keyboard())


@dp.callback_query_handler(Text(startswith="num_"))
async def callbacks_num(call: types.CallbackQuery):
    print(call.data)
    user_value = user_data.get(call.from_user.id, 0)
    action = call.data.split("_")[1]

    if action == "incr":
        user_data[call.from_user.id] = user_value+1
        await update_num_text(call.message, user_value+1)
    elif action == "decr":
        user_data[call.from_user.id] = user_value-1
        await update_num_text(call.message, user_value-1)
    elif action == "finish":
        await call.message.edit_text(f"Итого: {user_value}")

    await call.answer()


# Запуск бота
if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
